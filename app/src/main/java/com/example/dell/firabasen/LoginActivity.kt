package com.example.dell.firabasen


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*
import java.time.Instant

class LoginActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        var mAuth :FirebaseAuth =FirebaseAuth.getInstance()
log.setOnClickListener({



    var valid :Boolean=true
    var email=nemail.text.toString()
    var pass=npass.text.toString()

    if(email.isEmpty()){
        nemail.error="Please enter your email"
        valid=false
    }
    if(pass.isEmpty()){
        npass.error="Please enter your password"
        valid=false
    }

    if(valid){
        email=nemail.text.toString()
        pass=npass.text.toString()



        mAuth.signInWithEmailAndPassword(nemail.text.toString(),npass.text.toString()).addOnCompleteListener(this,{

            if(it.isSuccessful){
                var user:FirebaseUser? = mAuth.currentUser
                var intent =Intent(this,HomeActivity::class.java)
                startActivity(intent)
            }
        }).addOnFailureListener({
            Toast.makeText(this,it.message,Toast.LENGTH_LONG).show()
        })
}
})
    }
}
