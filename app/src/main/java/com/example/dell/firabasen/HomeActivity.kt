package com.example.dell.firabasen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            //Set on Home.
        } else {
            //Redirect to Login Page.
        }

    }
}
